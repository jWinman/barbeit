import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

for i in range(0,10):
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ux, uy = np.loadtxt("verteilung{}.csv".format(1000 * i), unpack = True)
        print(i)
        bins = [j for j in np.arange(-10, 10, 0.1)]
        counts1 = np.zeros(len(bins))
        counts2 = np.zeros(len(bins))

        for u in ux:
                for j in range(len(bins) - 1):
                        if (u > bins[j] and u < bins[j + 1]):
                                counts1[j] += 1
        for u in uy:
                for j in range(len(bins) - 1):
                        if (u > bins[j] and u < bins[j + 1]):
                                counts2[j] += 1
        
        print(sum(counts1))
        ax.plot(bins, counts1, "o", label="{} Zeitschritten".format(1000 * i))
        ax.set_ylabel("Häufigkeit")
        ax.set_xlabel("Geschwindigkeit $v_x$")
        ax.legend(loc="best")
        fig.savefig("verteilung{}.pdf".format(1000 * i))

x = np.linspace(-10, 10, 10000)
f = lambda x: np.sqrt(1) / np.sqrt(2 * np.pi) * np.exp(-0.5 * x**2)


