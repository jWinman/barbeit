import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

x, y = np.loadtxt("trajectory.csv", unpack = True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

ax.plot(x, y, "r.")
fig.savefig("trajectory.pdf")
