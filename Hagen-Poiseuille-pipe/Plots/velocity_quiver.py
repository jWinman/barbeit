import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize as sc
from uncertainties import *

x, y, ux, uy = np.loadtxt("../Daten/u.csv", unpack=True)
norm = np.sqrt(ux**2 + uy**2)

# quiver
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
ax.set_xlabel("$x / a$")
ax.set_ylabel("$y / a$")
norm = np.array(np.split(norm, x[-1] + 1)).T
im = ax.imshow(norm, origin="lower")
cb = fig.colorbar(im, ax = ax, orientation="horizontal")
cb.set_label(r"$v_{c,\mathrm{cm}} / \frac{a}{\updelta t}$")
fig.savefig("quiver.pdf")

# Parabolisches Geschwindigkeitsprofil
middle = np.floor((x[-1] + 1) / 2)
width = y[-1] + 1

i = 0
while(x[i] != middle):
    i += 1

y_correct = np.array([y[j + i] for j in range(2, len(y[i:width + i]) - 2)])
ux_correct = np.array([ux[j + i] for j in range(2, len(ux[i:width + i]) - 2)])

def func(x, a, b, c):
        return a * x**2 + b * x + c

popt, pcov = sc.curve_fit(func, y_correct, ux_correct)

a = ufloat((popt[0], pcov[0][0]))
b = ufloat((popt[1], pcov[1][1]))
c = ufloat((popt[2], pcov[2][2]))
y1 = np.linspace(0, 30, 1000)

print(a, b, c)

#Viskosität berechnen
eta = 10 * 0.01 / (2 * a)
print(eta)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(y[i:width + i], ux[i:width + i], "+", label="SRD-Messwerte")
ax.plot(y1, func(y1, a.nominal_value, b.nominal_value, c.nominal_value), label=r"$v_{c,\mathrm{cm}, x}(y) = %.2f \,\frac{1}{a \updelta t}\, y^2 + %.2f \,\frac{1}{\updelta t} \, y %.2f \, \frac{a}{\updelta t}$" %(a.nominal_value, b.nominal_value, c.nominal_value))
#ax.set_title("Geschwindigkeitsprofil bei $x = {}$".format(middle))
ax.legend(loc="lower center")
ax.set_xlim(-1, width + 1)
#ax.set_ylim(-0.1, 2)
ax.set_xlabel("$y / a$")
ax.set_ylabel(r"$v_{c,\mathrm{cm}, x}(y) / \frac{a}{\updelta t}$")
fig.savefig("u.pdf")
