#include <iostream>
#include <cstdio>
#include <cmath>
#include <random>
#include <cmath>

#include "cellgrid.h"
#include "particle.h"

using namespace std;



Particle* createparticles(int N, double xright, double yright, double T)
{
	default_random_engine generator;
	normal_distribution<double> distribution(0, 1);

	Particle *array = new Particle[N];
	for (int i = 0; i < N; i++)
	{
		array[i] = Particle( (double) rand() / (RAND_MAX + 1.0) * xright, (double) rand() / (RAND_MAX + 1.0) * yright, sqrt(T) * distribution(generator), sqrt(T) * distribution(generator), 1);
	}
	return array;
}

void work(int NP, double T, double force, double x, double y, double measure_time, double delta_t, double ***mean_x_y, double **particle_dens)
{
	// Teilchen und Zellen erstellen
	Particle *teilchenarray = createparticles(NP, x, y, T);
	CellGrid zellenarray(int(x), int(y), 1, NP);

	//mittlere Teilchenzahldichte
	int M = NP / (x * y);

	// Äquilibrierung
	clock_t t = clock();
	for (int w = 0; w < 1000; w++)
	{
		zellenarray.reset_all();
		zellenarray.fill_up_particle_array(teilchenarray, NP, x);
		zellenarray.acceleration_cell(teilchenarray, NP, delta_t, force, 0, y);
		zellenarray.mean(T, M);
		zellenarray.algorithm(0, x, 0, y, delta_t);
	/*	if (w % 1000 == 0)
		{
			printf("Output\n");
			char name[15];
			sprintf(name, "Daten/verteilung%d.csv", w);
			FILE *file = fopen(name, "w");
			for (int i = 0; i < NP; i++)
			{
				fprintf(file, "%.15f %.15f \n", teilchenarray[i].Getv('x'), teilchenarray[i].Getv('y'));
			}
		}*/
	}
	t = clock() - t;
	printf("%.5f\n", t / double(CLOCKS_PER_SEC));

	// Ausleseteil:
	for (int w = 0; w < measure_time; w++)
	{
		zellenarray.reset_all();
		zellenarray.fill_up_particle_array(teilchenarray, NP, x);
		zellenarray.acceleration_cell(teilchenarray, NP, delta_t, force, 0, y);
		zellenarray.mean(T, M);
		zellenarray.algorithm(0, x, 0, y, delta_t);
		zellenarray.reset_particle_array();
		zellenarray.fill_up_particle_array(teilchenarray, NP, x);
		zellenarray.mean(T, M);
		for (int i = 0; i < int(x); i++)
		{
			for (int j = 0; j < int(y) + 1; j++)
			{
				mean_x_y[i][j][0] += zellenarray.Getux(i, j) / measure_time;
				mean_x_y[i][j][1] += zellenarray.Getuy(i, j) / measure_time;
				particle_dens[i][j] += zellenarray.quantity(i, j) / (double) measure_time;
			}
		}
	}
	delete teilchenarray;
}

int main()
{
	// Initialisiert rand()
	srand((unsigned) time(NULL));
	
	// Randbedingungen etc. festlegen
	int NP = 30000;
	double T = 0.01;
	double x = 100, y = 30;
	int measure_time = 2000;
	double delta_t = 1;
	double force = 0.01;


	// Array in denen Mittelwert der Geschwindigkeiten bei der Auslesephase gespeichert werden
	double ***mean_x_y = new double**[int(x)]();
	double **particle_dens = new double*[int(x)]();
	for (int i = 0; i < int(x); i++)
	{
		mean_x_y[i] = new double*[int(y) + 1]();
		particle_dens[i] = new double[int(y) + 1]();
		for (int j = 0; j < int(y) + 1; j++)
		{
			mean_x_y[i][j] = new double[2]();

		}
	}

	//Äquilibrierung u. Messung
	work(NP, T, force, x, y, measure_time, delta_t, mean_x_y, particle_dens);

	printf("Output\n");
	char name1[15];
	char name2[20];
	sprintf(name1, "Daten/u.csv");
	sprintf(name2, "Daten/dichte.csv");
	FILE *file1 = fopen(name1, "w");
	FILE *file2 = fopen(name2, "w");
	for (int i = 0; i < int(x); i++)
	{
		for (int j = 0; j < int(y) + 1; j++)
		{
		fprintf(file1, "%d %d %.15f %.15f\n", i, j, mean_x_y[i][j][0], mean_x_y[i][j][1]);
		fprintf(file2, "%.2f ", particle_dens[i][j]);
		}
		fprintf(file2, "\n");
	}
	fclose(file1);
	fclose(file2);

	return 0;
}
