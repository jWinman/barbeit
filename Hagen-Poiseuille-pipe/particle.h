#ifndef PARTICLE_H
#define PARTICLE_H

class Particle 
{
	private:
		double _x, _y, _mass;
		double _v_x, _v_y;

	public:
		Particle() {};
		Particle(double x, double y, double v_x, double v_y, double mass) : _x(x), _y(y), _mass(mass), _v_x(v_x), _v_y(v_y) {}
		double GetPos(char pos);
		double Getv(char pos); 
		double Getmass() {return _mass;}
		void x_border(double xleft, double xright);
		void y_border(double yleft, double yright, double time);
		void streaming(double time);
		void acceleration(double time, double force, double ydown, double yup);
		void collisionSRD(double vx_mittel, double vy_mittel, int R);
		void shift(double a0_x, double a0_y, double xleft, double xright);
};
#endif
