import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

NP = ['N', 'Y']
time = np.arange(0, 15)
M = [np.zeros(len(time)) for i in np.arange(len(NP))]

k = 0
for i in NP[0]:
        for j in time:
                density = np.loadtxt("../Daten/dichte-{}-{}.csv".format(i, j))
                fig = plt.figure()
                ax = fig.add_subplot(1, 1, 1)
                im = ax.imshow(density, origin="lower", interpolation="nearest")
                cb = fig.colorbar(im, ax = ax)
                cb.set_label(r"mittlere Teilchenzahl $M$")
                fig.savefig("dichte-{}-{}.pdf".format(i, j))
        k += 1
