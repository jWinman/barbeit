#include <cmath>
#include <iostream>
#include <random>
#include <functional>

#include "particle.h"
#include "cellgrid.h"

// Konstruktor
CellGrid::CellGrid(int Nx, int Ny, int a, int N_Particle) : _Nx(Nx + 1), _Ny(Ny + 1), _a(a), _N_Particle(N_Particle)
{
	// Speicher anfordern
	_particle_array = new Particle***[_Nx]();
	_u = new double**[_Nx]();
	_Particle_per_cell = new int*[_Nx]();
	_R = new int*[_Nx]();
//	_AB = new double**[_Nx]();
	for (int i = 0; i < _Nx; i++)
	{
		_particle_array[i] = new Particle**[_Ny]();
		_u[i] = new double*[_Ny]();
		_Particle_per_cell[i] = new int[_Ny]();
		_R[i] = new int[_Ny]();
//		_AB[i] = new double*[_Ny]();
		for (int j = 0; j < _Ny; j++)
		{
			_particle_array[i][j] = new Particle*[int(_N_Particle)]();
			_u[i][j] = new double[2]();
//			_AB[i][j] = new double[2]();
		}
	}
}

void CellGrid::ghost_particles(int i, int j, double T, double M)
{
	using namespace std;
	double sigma = sqrt((M - _Particle_per_cell[i][j]) * T);
	default_random_engine generator(rand());
	normal_distribution<double> distribution(0, sigma);
	_u[i][j][0] = (_u[i][j][0] + distribution(generator)) / M;
	_u[i][j][1] = (_u[i][j][1] + distribution(generator)) / M;
}

void CellGrid::reset_all()
{
	// Nullsetzen und Startwerte setzen
	_shift_x = _a / 2. - rand() / (RAND_MAX + 1.0);
	_shift_y = _a / 2. - rand() / (RAND_MAX + 1.0);
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			_Particle_per_cell[i][j] = 0;
			_R[i][j] = ((int) rand() % 2 == 0) ? -1 : 1;
		}
	}
}

void CellGrid::reset_particle_array()
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			_Particle_per_cell[i][j] = 0;
		}
	}
}

void CellGrid::drive_all(Particle *array, int length, double time, double force, double variance)
{
	using namespace std;
	default_random_engine generator(rand());
	normal_distribution<double> distribution(0, variance);
	for (int k = 0; k < length; k++)
	{
		array[k].drive(time, force, distribution(generator));
	}
}

void CellGrid::fill_up_particle_array(Particle *array, int length)
{
	for (int k = 0; k < length; k++) // Schleife über alle Teilchen
	{
		// Auffüllen in _particle_array unter Beachtung des shifts
		array[k].shift(_shift_x, _shift_y); // shiften
		int tmpx = 0;
		if (_shift_x < 0)
		{
			tmpx = array[k].GetPos('x') / _a + ceil(fabs(_shift_x));
		}
		else
		{
			tmpx = array[k].GetPos('x') / _a;
		}
		
		int tmpy = 0;
		if (_shift_y < 0)
		{
			tmpy = array[k].GetPos('y') / _a + ceil(fabs(_shift_y));
		}
		else
		{
			tmpy = array[k].GetPos('y') / _a;
		}

		_particle_array[tmpx][tmpy][_Particle_per_cell[tmpx][tmpy]] = &array[k];
		_Particle_per_cell[tmpx][tmpy]++;
		array[k].shift(-_shift_x, -_shift_y); //zurückshiften
	}
}

void CellGrid::mean(double T, double M, double radius)
{
	// Berechnung von u, Ghost-Particles hinzufügen
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		int border_plus = floor(radius + sqrt(2 * radius * i - i * i));
		int border_minus = floor(radius - sqrt(2 * radius * i - i * i));
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			_u[i][j][0] = _u[i][j][1] = 0;
			for (int k = 0; k < _Particle_per_cell[i][j]; k++) // Schleife über Teilchen in der Zelle (i, j)
			{
				_u[i][j][0] += _particle_array[i][j][k]->Getv('x');
				_u[i][j][1] += _particle_array[i][j][k]->Getv('y');
			}

			if ((j == border_plus || j == border_minus) && M > _Particle_per_cell[i][j] && _Particle_per_cell[i][j] > 0)
			{
				ghost_particles(i, j, T, M); // Ghost- / Wall-particles hinzufügen
			}
			else if (_Particle_per_cell[i][j] > 0)
			{
				_u[i][j][0] /= _Particle_per_cell[i][j];
				_u[i][j][1] /= _Particle_per_cell[i][j];
			}
		}
	}
}

void CellGrid::algorithm(double radius, double time)
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			for (int k = 0; k < _Particle_per_cell[i][j]; k++) // Schleife über alle Teilchen in der Zelle (i, j)
			{
				_particle_array[i][j][k]->collisionSRD(_u[i][j][0], _u[i][j][1], _R[i][j]);
				_particle_array[i][j][k]->streaming(time);
				_particle_array[i][j][k]->circle_border(radius, time);
			}
		}
	}
}

CellGrid::~CellGrid()
{
	for (int i = 0; i < _Nx; i++)
	{
		for (int j = 0; j < _Ny; j++)
		{
			delete[] _particle_array[i][j];
			delete[] _u[i][j];
		}
		delete[] _particle_array[i];
		delete[] _u[i];
		delete[] _Particle_per_cell[i];
		delete[] _R[i];
	}
	delete[] _particle_array;
	delete[] _u;
	delete[] _Particle_per_cell;
	delete[] _R;
}
