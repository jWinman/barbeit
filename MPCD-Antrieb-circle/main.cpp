#include <iostream>
#include <cstdio>
#include <cmath>
#include <random>

#include "cellgrid.h"
#include "particle.h"

using namespace std;



Particle* createparticles(int N, double radius, double T)
{
	default_random_engine generator;
	normal_distribution<double> distribution(0, 1);

	Particle *array = new Particle[N];
	for (int i = 0; i < N; i++)
	{
		double r = (double) rand() / (RAND_MAX + 1.0) * radius;
		double phi = 2 * M_PI * double(rand()) / (RAND_MAX);
		array[i] = Particle(r * cos(phi) + radius, r * sin(phi) + radius, sqrt(T) * distribution(generator), sqrt(T) * distribution(generator), 1, 2 * M_PI * rand() / (RAND_MAX + 1.0));
	}
	return array;
}

void work(int NP, double T, double delta_t, double force, double variance, double radius, double x, double y, int measure_time, int measure_intervall, double ****mean_x_y, double ***particle_dens, double *theta_variance, double *n_variance, double *r_variance, double *r_mean_x, double *r_mean_y, double ***omega)
{
	srand((unsigned) time(NULL));

	// Teilchen und Zellen erstellen
	Particle *teilchenarray = createparticles(NP, radius, T);
	CellGrid zellenarray(int(ceil(x)),int(ceil(y)), 1, NP);

	// mittlere Teilchenzahl	
	double M = (double) NP / (M_PI * radius * radius);

	// Ausleseteil:
	double *theta_0 = new double[NP]();
	double *x_0 = new double[NP]();
	double *y_0 = new double[NP]();
	size_t time = 0;

	// Ausleseteil:
	for (int h = 0; h < measure_intervall; h++)
	{
		for (int w = 0; w < 5000; w++)
		{
			zellenarray.reset_all();
			zellenarray.fill_up_particle_array(teilchenarray, NP);
			zellenarray.drive_all(teilchenarray, NP, delta_t, force, variance);
			zellenarray.mean(T, M, radius);
			zellenarray.algorithm(radius, delta_t);
		}
		
		for (int w = 0; w < measure_time; w++)
		{
			zellenarray.reset_all();
			zellenarray.fill_up_particle_array(teilchenarray, NP);
			zellenarray.drive_all(teilchenarray, NP, delta_t, force, variance);
			zellenarray.mean(T, M, radius);
			zellenarray.algorithm(radius, delta_t);
			zellenarray.reset_particle_array();
			zellenarray.fill_up_particle_array(teilchenarray, NP);
			zellenarray.mean(T, M, radius);

			// Daten in Arrays schreiben
			// Varianzen berechnen
			for (int k = 0; k < NP; k++)
			{
				if (h == 0 && w == 0)
				{
					theta_0[k] = teilchenarray[k].Get_theta();
					x_0[k] = teilchenarray[k].GetPos('x');
					y_0[k] = teilchenarray[k].GetPos('y');
				}
				theta_variance[time] += (teilchenarray[k].Get_theta() - theta_0[k]) * (teilchenarray[k].Get_theta() - theta_0[k]) / NP;
				n_variance[time] += cos(teilchenarray[k].Get_theta() - theta_0[k]) / NP;
				r_variance[time] += ((teilchenarray[k].GetPos('x') - x_0[k]) * (teilchenarray[k].GetPos('x') - x_0[k]) + (teilchenarray[k].GetPos('y') - y_0[k]) * (teilchenarray[k].GetPos('y') - y_0[k])) / NP;
				r_mean_x[time] += (teilchenarray[k].GetPos('x') - x_0[k]) / NP ;
				r_mean_y[time] += (teilchenarray[k].GetPos('y') - y_0[k]) / NP;
			}
			time++;

			//Geschwindigkeitsfeld erstellen
/*			for (int i = 0; i < int(x); i++)
			{
				for (int j = 0; j < int(y) + 1; j++)
				{
					mean_x_y[h][i][j][0] += zellenarray.Getux(i, j) / measure_time;
					mean_x_y[h][i][j][1] += zellenarray.Getuy(i, j) / measure_time;
					particle_dens[h][i][j] += zellenarray.quantity(i, j) / (double) measure_time;
					if (i < int(x) - 1 && j < int(y))
					{
						omega[h][i][j] += (zellenarray.Getuy(i + 1, j) - zellenarray.Getuy(i, j) - (zellenarray.Getux(i, j + 1) - zellenarray.Getux(i, j))) / (measure_time);
					}
				}
			}
*/		}
	}
	delete teilchenarray;
}

int main(int argc, char *argv[])
{
	// Initialisiert rand()
	srand((unsigned) time(NULL));
	
	// Anfangs- u. Randbedingungen festlegen
	int NP = 30000;
	double T = 0.01;
	double R = sqrt(NP / (10 * M_PI));
	double x = 2 * R, y = 2 * R;
	int measure_time = 1000;
	int measure_intervall = 25;
	double delta_t = 1;
	double force = atof(argv[1]); 
	double variance = 0.01;

	// Array in denen Mittelwert der Geschwindigkeiten bei der Auslesephase gespeichert werden
	double *theta_variance = new double[measure_intervall * measure_time]();
	double *n_variance = new double[measure_intervall * measure_time]();
	double *r_variance = new double[measure_intervall * measure_time]();
	double *r_mean_x = new double[measure_intervall * measure_time]();
	double *r_mean_y = new double[measure_intervall * measure_time]();

	double ****mean_x_y = new double***[measure_intervall]();
	double ***particle_dens = new double**[measure_intervall]();
	double ***omega = new double**[measure_intervall]();

	for (int h = 0; h < measure_intervall; h++)
	{
		mean_x_y[h] = new double**[int(x) + 1];
		particle_dens[h] = new double*[int(x) + 1];
		omega[h] = new double*[int(x)];
		for (int i = 0; i < int(x) + 1; i++)
		{
			mean_x_y[h][i] = new double*[int(y) + 1]();
			particle_dens[h][i] = new double[int(y) + 1]();
			omega[h][i] = new double[int(y) + 1]();
			for (int j = 0; j < int(y) + 1; j++)
			{
				mean_x_y[h][i][j] = new double[2]();

			}
		}
	}

	//Äquilibrieren und Messen
	work(NP, T, delta_t, force, variance, R, x, y, measure_time, measure_intervall, mean_x_y, particle_dens, theta_variance, n_variance, r_variance, r_mean_x, r_mean_y, omega);

	//Output
	printf("Output\n");
	for (int h = 0; h < measure_intervall; h++)
	{
		// Geschwindigkeit u in Datei schreiben
		char name1[256];
		char name2[256];
		char name3[256];
		sprintf(name1, "Daten/u%d.csv", h);
		sprintf(name2, "Daten/dichte%d.csv", h);
		sprintf(name3, "Daten/omega%d.csv", h);
		FILE *file1 = fopen(name1, "w");
		//FILE *file2 = fopen(name2, "w");
		FILE *file3 = fopen(name3, "w");
		for (int i = 0; i < int(x); i++)
		{
			for (int j = 0; j < int(y) + 1; j++)
			{
				fprintf(file1, "%d %d %.15f %.15f\n", i, j, mean_x_y[h][i][j][0], mean_x_y[h][i][j][1]);
		//		fprintf(file2, "%.20f ", particle_dens[h][i][j]);
				if (i < int(x) - 1 && j < int(y))
				{
					fprintf(file3, "%d %d %.15f\n", i, j, omega[h][i][j]);
				}
			}
			//fprintf(file2, "\n");
		}
		fclose(file1);
		//fclose(file2);
		fclose(file3);
	}

	// Varianzen in Datei schreiben
	char name4[256];
	char name5[256];
	char name6[256];
	sprintf(name4, "Daten/theta_variance%s.csv", argv[1]);
	sprintf(name5, "Daten/n_variance%s.csv", argv[1]);
	sprintf(name6, "Daten/r_variance%s.csv", argv[1]);
	FILE *file4 = fopen(name4, "w");
	FILE *file5 = fopen(name5, "w");
	FILE *file6 = fopen(name6, "w");
	for (int time = 0; time < measure_intervall * measure_time; time++)
	{
		fprintf(file4, "%.10f\n", theta_variance[time]);
		fprintf(file5, "%.10f\n", n_variance[time]);
		fprintf(file6, "%.10f\n", r_variance[time]);
	}
	fclose(file4);
	fclose(file5);
	fclose(file6);
	return 0;
}
