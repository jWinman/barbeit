#include <cmath>
#include <iostream>

#include "particle.h"

double Particle::GetPos(char pos)
{
	switch(pos)
	{
		case 'x': return _x;
		case 'y': return _y;
		default: return 0;
	}
}

double Particle::Getv(char pos)
{
	switch(pos)
	{
		case 'x': return _v_x;
		case 'y': return _v_y;
		default: return  0;
	}
}

void Particle::circle_border(double radius, double time)
{
	double norm = sqrt((_x - radius) * (_x - radius) + (_y - radius) * (_y - radius));
	if (norm >= radius)
	{
		_v_x = -_v_x;
		_v_y = -_v_y;
		streaming(time);
	}
}

void Particle::streaming(double time)
{
	_y += time * _v_y;
	_x += time * _v_x;
}

void Particle::drive(double time, double force, double theta)
{
	_theta += theta;
	_v_x += force / _mass * cos(_theta) * time;
	_v_y += force / _mass * sin(_theta) * time;
}

void Particle::collisionSRD(double vx_mittel, double vy_mittel, int R)
{
	double v_x_temp = _v_x;
	_v_x = vx_mittel - R * (_v_y - vy_mittel);
	_v_y = vy_mittel + R * (v_x_temp - vx_mittel);
}

/*void Particle::collisionSRD_ang_mom(double vx_mittel, double vy_mittel, double A, double B, int R)
{
	double v_x_temp = _v_x;
	if (A == 0 && B == 0)
	{
		_v_x = vx_mittel - R * (_v_y - vy_mittel);
		_v_y = vy_mittel + R * (v_x_temp - vx_mittel);
	}
	else
	{
		double sin_alpha = 2 * A * B / (A * A + B * B);
		double cos_alpha = (A * A - B * B) / (A * A + B * B);
		_v_x = vx_mittel + R * (cos_alpha * (v_x_temp - vx_mittel) - sin_alpha * (_v_y - vy_mittel)); 
		_v_y = vy_mittel + R * (sin_alpha * (v_x_temp - vy_mittel) + cos_alpha * (_v_y - vy_mittel));
	}
}*/

void Particle::shift(double a0_x, double a0_y)
{
	_x += a0_x;
	_y += a0_y;
}













