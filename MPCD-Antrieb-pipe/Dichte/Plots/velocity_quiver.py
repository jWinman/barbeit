import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.optimize as sc
from uncertainties import *

NP = ['Y', 'N']
time = np.arange(0, 15)

for j in NP[1]:
        for i in time:
                # quiver
                x, y, ux, uy = np.loadtxt("../Daten/u{}-{}.csv".format(j, i), unpack=True)
                norm = np.sqrt(ux**2 + uy**2)

                fig = plt.figure()
                fig.set_tight_layout(True)
                ax = fig.add_subplot(1, 1, 1)
                ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
                norm = np.array(np.split(norm, x[-1] + 1)).T
                im = ax.imshow(norm, origin="lower")
                cb = fig.colorbar(im, ax = ax, orientation="horizontal")
                fig.savefig("quiver{}.pdf".format(i))
