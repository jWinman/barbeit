import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

alpha = np.arange(0.005, 0.1, 0.005)
omega_square = []

for i in alpha:
        f, time, omega2 = np.loadtxt("../Daten-cst-variance/omega_square%.3f.csv" %i, unpack=True)
        omega2 = omega2
        omega_square.append(omega2)

omega_square = np.array(omega_square)

#Plot alpha - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(alpha, omega_square[:,0], ".")
ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$\langle \overline{\omega^2} \rangle$")
fig.savefig("alpha-omega_square.pdf")

#Plot Zeit - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, omega_square[2], ".")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\langle \overline{\omega}^2 \rangle$")
fig.savefig("time-omega_square.pdf")

#Plot Zeit & alpha - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
im = ax.imshow(omega_square[:,:20], interpolation="nearest", origin="lower")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\alpha$")
plt.xticks([0, 5, 10, 15], [0, r"$5\cdot6000$", r"$10\cdot6000$", r"$15\cdot6000$"])
plt.yticks([0, 5, 10, 15], [0, 0.25, 0.5, 0.75])
cb = fig.colorbar(im, ax = ax)
cb.set_label(r"$\langle \omega(\alpha, t)^2 \rangle$")
fig.savefig("omega_square3d.pdf")
