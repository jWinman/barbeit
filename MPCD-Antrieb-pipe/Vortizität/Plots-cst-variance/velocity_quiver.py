import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

alpha = "0.100"
for i in np.arange(50):
        # quiver
        x, y, ux, uy = np.loadtxt("../Daten-cst-variance/u{}-{}.csv".format(alpha, i), unpack=True)
        x_o, y_o, omega = np.loadtxt("../Daten-cst-variance/omega{}-{}.csv".format(alpha, i), unpack=True)
        omega = omega
        norm = np.sqrt(ux**2 + uy**2)
        omega = np.array(np.split(omega, x_o[-1] + 1)).T

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
        ax.set_xlabel("$x / a$")
        ax.set_ylabel("$y / a$")
        norm = np.array(np.split(norm, x[-1] + 1)).T
        im = ax.imshow(norm, origin="lower")
        cb = fig.colorbar(im, ax = ax, orientation="horizontal")
        cb.set_label(r"$v_{c,\mathrm{cm}} / \frac{a}{\updelta t}$")
        fig.savefig("quiver{}-{}.pdf".format(alpha, i))
