import numpy as np
import matplotlib.pyplot as plt

alpha = np.arange(50, 100, 5)
omega_square = []

for i in alpha:
        f, time, omega2 = np.loadtxt("../Daten-cst-variance2/omega_square%d.csv" %i, unpack=True)
        omega_square.append(omega2)

omega_square = np.array(omega_square)

#Plot alpha - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(alpha, omega_square[:,0], ".")
ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$\langle \overline{\omega^2} \rangle$")
fig.savefig("alpha-omega_square.pdf")

#Plot Zeit - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, omega_square[0], ".")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\langle \overline{\omega}^2 \rangle$")
fig.savefig("time-omega_square.pdf")

#Plot Zeit & alpha - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
im = ax.imshow(omega_square, interpolation="nearest", origin="lower")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\alpha$")
cb = fig.colorbar(im, ax = ax)
cb.set_label(r"$\langle \overline{\omega(\alpha, t)}^2 \rangle$")
fig.savefig("omega_square3d.pdf")
