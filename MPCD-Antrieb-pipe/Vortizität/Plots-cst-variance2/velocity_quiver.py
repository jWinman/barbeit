import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

alpha = "100"
for i in np.arange(50):
        # quiver
        x, y, ux, uy = np.loadtxt("../Daten-cst-variance2/u{}-{}.csv".format(i, alpha), unpack=True)
        x_o, y_o, omega = np.loadtxt("../Daten-cst-variance2/omega{}-{}.csv".format(i, alpha), unpack=True)
        omega = omega
        norm = np.sqrt(ux**2 + uy**2)
        omega = np.array(np.split(omega, x_o[-1] + 1)).T

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
        norm = np.array(np.split(norm, x[-1] + 1)).T
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")
        im = ax.imshow(norm, origin="lower")
        cb = fig.colorbar(im, ax = ax, orientation="horizontal")
        cb.set_label(r"$\omega^2$")
        fig.savefig("quiver{}-{}.pdf".format(alpha, i))
