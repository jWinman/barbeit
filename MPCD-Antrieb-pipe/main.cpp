#include <iostream>
#include <cstdio>
#include <cmath>
#include <random>

#include "cellgrid.h"
#include "particle.h"

using namespace std;



Particle* createparticles(int N, double xright, double yright, double T)
{
	srand((unsigned) time(NULL));
	default_random_engine generator;
	normal_distribution<double> distribution(0, 1);

	Particle *array = new Particle[N];
	for (int i = 0; i < N; i++)
	{
		array[i] = Particle((double) rand() / (RAND_MAX + 1.0) * xright, (double) rand() / (RAND_MAX + 1.0) * yright, sqrt(T) * distribution(generator), sqrt(T) * distribution(generator), 1, 0);
	}
	return array;
}

void work(int NP, double T, double delta_t, double alpha, double variance, double x, double y, int measure_time, int measure_intervall, double ****mean_x_y, double ***particle_dens, double ***omega, bool on_off)
{
	srand((unsigned) time(NULL));
	// Teilchen und Zellen erstellen
	Particle *teilchenarray = createparticles(NP, x, y, T);
	CellGrid zellenarray(int(x), int(y), 1, NP);

	// mittlere Teilchenzahl	
	double M = (double) NP / (x * y);

	// Ausleseteil:
	for (int h = 0; h < measure_intervall; h++)
	{
		// Äquilibrierung
		for (int w = 0; w < 5000; w++)
		{
			zellenarray.reset_all();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.drive_all(teilchenarray, NP, delta_t, alpha, variance);
			zellenarray.mean(T, M);
			zellenarray.algorithm(0, x, 0, y, delta_t, on_off);
		}

		for (int w = 0; w < measure_time; w++)
		{
			// Algorithmus nochmal ausführen
			zellenarray.reset_all();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.drive_all(teilchenarray, NP, delta_t, alpha, variance);
			zellenarray.mean(T, M);
			zellenarray.algorithm(0, x, 0, y, delta_t, on_off);
			zellenarray.reset_particle_array();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.mean(T, M);

			//Geschwindigkeitsfeld erstellen
			for (int i = 0; i < int(x); i++)
			{
				for (int j = 0; j < int(y) + 1; j++)
				{
					mean_x_y[h][i][j][0] += zellenarray.Getux(i, j) / measure_time;
					mean_x_y[h][i][j][1] += zellenarray.Getuy(i, j) / measure_time;
					particle_dens[h][i][j] += zellenarray.quantity(i, j) / ((double) measure_time);
					if (i < int(x) - 1 && j < int(y))
					{
						double omega_tmp = (zellenarray.Getuy(i + 1, j) - zellenarray.Getuy(i, j)) - (zellenarray.Getux(i, j + 1) - zellenarray.Getux(i, j));
						omega[h][i][j] += omega_tmp / measure_time;
					}
				}
			}
		}
	}
	delete teilchenarray;
}

int main(int argc, char *argv[])
{
	// Initialisiert rand()
	srand((unsigned) time(NULL));
	
	// Anfangs- u. Randbedingungen festlegen
	int NP = 30000;
	double T = 0.01;
	double x = 100, y = 30;
	int measure_time = 1000;
	int measure_intervall = 5;
	double delta_t = 1;
	double alpha = 0.01;
	double variance = 0.01;
	bool on_off = true;
/*	if (*argv[1] == 'Y')
	{
		on_off = true;
	}
	else if (*argv[1] == 'N')
	{
		on_off = false;
	}
*/
	// Array in denen Mittelwert der Geschwindigkeiten bei der Auslesephase gespeichert werden
	double ****mean_x_y = new double***[measure_intervall]();
	double ***particle_dens = new double**[measure_intervall]();
	double ***omega = new double**[measure_intervall]();
	double *omega_square = new double[measure_intervall]();

	for (int h = 0; h < measure_intervall; h++)
	{
		mean_x_y[h] = new double**[int(x)];
		particle_dens[h] = new double*[int(x)]();
		omega[h] = new double*[int(x)];
		for (int i = 0; i < int(x); i++)
		{
			mean_x_y[h][i] = new double*[int(y) + 1]();
			particle_dens[h][i] = new double[int(y) + 1]();
			omega[h][i] = new double[int(y) + 1]();
			for (int j = 0; j < int(y) + 1; j++)
			{
				mean_x_y[h][i][j] = new double[2]();

			}
		}
	}

	//Äquilibrieren und Messen
	work(NP, T, delta_t, alpha, variance, x, y, measure_time, measure_intervall, mean_x_y, particle_dens, omega, on_off);

	//Output
	printf("Output\n");
	char name1[256];
	sprintf(name1, "Daten/omega_square%s.csv", argv[1]);
	FILE *file1 = fopen(name1, "w");
	for (int h = 0; h < measure_intervall; h++)
	{
		char name2[256];
		char name3[256];
		char name4[256];
		sprintf(name2, "Daten/u%s-%d.csv", argv[1], h);
		sprintf(name3, "Daten/omega%s-%d.csv", argv[1], h);
		sprintf(name4, "Daten/dichte%s-%d.csv", argv[1], h);
		FILE *file2 = fopen(name2, "w");
		FILE *file3 = fopen(name3, "w");
		FILE *file4 = fopen(name4, "w");
		for (int i = 0; i < int(x); i++)
		{
			for (int j = 0; j < int(y) + 1; j++)
			{
				fprintf(file2, "%d %d %.15f %.15f\n", i, j, mean_x_y[h][i][j][0], mean_x_y[h][i][j][1]);
				fprintf(file4, "%.20f ", particle_dens[h][i][j]);
				if (i < int(x) - 1 && j < int(y))
				{
					fprintf(file3, "%d %d %.15f\n", i, j, omega[h][i][j] * omega[h][i][j]);
					omega_square[h] += omega[h][i][j] * omega[h][i][j] / ((int(x) - 1) * int(y));
				}
			}
			fprintf(file4, "\n");
		}
		fclose(file2);
		fclose(file3);
		fclose(file4);
		fprintf(file1, "%s %d %.20f\n", argv[1], h, omega_square[h]);
	}
	fclose(file1);
	return 0;
}
