#include <cmath>

#ifndef PARTICLE_H
#define PARTICLE_H

class Particle 
{
	private:
		double _x, _y, _mass;
		double _v_x, _v_y;
		double _theta;

	public:
		Particle() {};
		Particle(double x, double y, double v_x, double v_y, double mass, double theta) :
			_x(x), 
			_y(y), 
			_mass(mass), 
			_v_x(v_x), 
			_v_y(v_y), 
			_theta(theta) {}
		double GetPos(char pos);
		double Getv(char pos); 
		double Get_axial_x() {return cos(_theta);};
		double Get_axial_y() {return sin(_theta);};
		double Get_theta() {return _theta;};
		double Getmass() {return _mass;}
		void x_border(double xleft, double xright);
		void y_border(double yleft, double yright, double time);
		void streaming(double time);
		void acceleration(double time, double force);
		void drive(double time, double force, double theta);
		void collisionSRD(double vx_mittel, double vy_mittel, int R);
		void shift(double a0_x, double a0_y, double xleft, double xright);
};
#endif
