import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

NP = ['N', 'Y']
time = np.arange(0, 3)
M = [np.zeros(len(time)) for i in np.arange(len(NP))]

for i in NP:
        for j in time:
                density = np.loadtxt("../Daten1/dichte{}-{}.csv".format(i, j), unpack=True)
                print(len(density[1]))
                fig = plt.figure()
                ax = fig.add_subplot(1, 1, 1)
                ax.set_xlabel("$x / a$")
                ax.set_ylabel("$y / a$")
                im = ax.imshow(density, origin="lower", interpolation="nearest")
                cb = fig.colorbar(im, ax = ax, orientation="horizontal")
                cb.set_label(r"$N_c$")
                fig.savefig("dichte-{}-{}.pdf".format(i, j))
