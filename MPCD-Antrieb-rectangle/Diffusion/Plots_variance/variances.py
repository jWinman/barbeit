import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize as sc
from uncertainties import *

alpha = "0.020"
theta_variance = np.loadtxt("../Daten_variance/theta_variance{}.csv".format(alpha))
n_variance = np.loadtxt("../Daten_variance/n_variance{}.csv".format(alpha))
r_variance = np.loadtxt("../Daten_variance/r_variance{}.csv".format(alpha)) 

time = np.arange(len(theta_variance))

# Fits für theta, n, r

f = lambda x, a: a * x
g = lambda x, a: np.exp(- a * x)

popt, pcov = sc.curve_fit(f, time, theta_variance)
a = ufloat((popt[0], pcov[0][0]))
print(a)

popt, pcov = sc.curve_fit(g, time, n_variance)
b = ufloat((popt[0], pcov[0][0]))
print(b)

popt, pcov = sc.curve_fit(f, time, r_variance)
c = ufloat((popt[0], 0))
print(c)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, theta_variance, ".")
ax.plot(time, f(time, a.nominal_value), label=r"$\langle(\theta(t) - \theta(0))^2\rangle = {:.5f}\cdot t$".format(a.nominal_value))
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\langle(\theta(t) - \theta(0))^2\rangle$")
ax.legend(loc="best")
fig.savefig("theta_variance{}.pdf".format(alpha))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, n_variance, ".")
ax.plot(time, g(time, b.nominal_value), label=r"$\langle\vec{n}(t)\vec{n}(0)\rangle = \exp(- %.5f t)$" %b.nominal_value)
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\langle\vec{n}(t)\vec{n}(0)\rangle$")
ax.legend(loc="best")
fig.savefig("n_variance{}.pdf".format(alpha))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, r_variance, ".")
ax.plot(time, f(time, c.nominal_value), label=r"$\langle(\vec{r}(t) - \vec{r}(0))^2\rangle - \langle\vec{r}(t) - \vec{r}(0)\rangle^2 = %.3f t$" %c.nominal_value)
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\langle(\vec{r}(t) - \vec{r}(0))^2\rangle - \langle\vec{r}(t) - \vec{r}(0)\rangle^2$")
ax.legend(loc="best")
fig.savefig("r_variance{}.pdf".format(alpha))
