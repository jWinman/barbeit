import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize as sc
import uncertainties as uc
from matplotlib.font_manager import FontProperties

fontP1 = FontProperties()
fontP1.set_size('x-small')

alpha_string = ["0.005", "0.010", "0.020", "0.030", "0.040", "0.050", "0.060", "0.070", "0.080", "0.090", "0.100"]
r = lambda a,t: a * t

#Theta-Varianz
fig = plt.figure()
ax1 = fig.add_subplot(3, 1, 1)
theta_variance = np.loadtxt("../Daten_variance/theta_variance0.005.csv")
time = np.arange(len(theta_variance))

popt, pcov = sc.curve_fit(r, time, theta_variance)
a = uc.ufloat((popt[0], pcov[0][0]))

ax1.plot(time[:1000], r(a.nominal_value, time[:1000]), "r-", label=r"$\left\langle(\theta(t' + t) - \theta(t'))^2\right\rangle = {:.4f} \frac{{t}}{{\updelta t}}$".format(a.nominal_value))
ax1.plot(time[:1000], theta_variance[:1000], "b--", label="SRD-Messwerte")
ax1.set_ylabel(r"$\left\langle(\theta(t' + t) - \theta(t'))^2\right\rangle$")
#ax1.set_ylim(0, 0.15)
ax1.legend(loc="best")

#n Varianz
g = lambda x, a: np.exp(- a * x)

ax2 = fig.add_subplot(3, 1, 2)
n_variance = np.loadtxt("../Daten_variance/n_variance0.005.csv")
time = np.arange(len(n_variance))

popt, pcov = sc.curve_fit(g, time, n_variance)
b = uc.ufloat((popt[0], pcov[0][0]))

ax2.plot(time[:1000], n_variance[:1000], "r-", label=r"$\langle\bm{n}(t' + t)\bm{n}(t')\rangle = \exp(- %.5f \frac{t}{\updelta t})$" %b.nominal_value)
ax2.plot(time[:1000], n_variance[:1000], "b--", label="SRD-Messwerte")
ax2.set_ylabel(r"$\langle\bm{n}(t' + t)\bm{n}(t')\rangle$")
#ax2.set_ylim(0.92, 1.0)
ax2.legend(loc="best")

#r-Varianz - Plot und fit
ax3 = fig.add_subplot(3, 1, 3)
ax3.set_xlabel(r"$t / \updelta t$")
ax3.set_ylabel(r"$\left\langle(\bm{r}(t' + t) - \bm{r}(t'))^2\right\rangle / a^2$")

D = []
alpha = []
colors = ['#000000', '#0D00FF', '#FF0000', '#FF00C4', '#0F9600', '#E68A00', '#00FFFF', '#E5FF01', '#600064', '#6FFF00', '#666666']
j = 0
for i in alpha_string:
        r_variance = np.loadtxt("../Daten_variance/r_variance{}.csv".format(i))
        time = np.arange(len(n_variance))
        popt, pcov = sc.curve_fit(r, time[:100], r_variance[:100])
        a = uc.ufloat((popt[0], pcov[0][0]))
#        ax.plot(time, r(a.nominal_value, time))
        D.append(a.nominal_value)
        alpha.append(float(i))
        ax3.plot(time[:1000], r_variance[:1000], "-", color = colors[j], label=r"$\alpha = {}$".format(i))
        #ax3.set_ylim(0, 1)
        j += 1
ax3.legend(loc="best", ncol=2, prop=fontP1)
fig.savefig("variance_rectangle.pdf")

D = np.array(D)
alpha = np.array(alpha)
D_alpha = lambda x, a, b: a * x*x + b
alpha_conti = np.linspace(0, 0.1083, 1000)

popt, pcov = sc.curve_fit(D_alpha, alpha, D, )
D1 = uc.ufloat((popt[0], pcov[0][0]))
D2 = uc.ufloat((popt[1], pcov[1][1]))

print(D[0])
print(D1, D2)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_xlabel(r"$\alpha / \frac{m_i a}{\updelta t^2}$")
ax.set_ylabel(r"$D(\alpha) / \frac{a^2}{\updelta t}$")
ax.plot(alpha, D, "+", label=r"SRD-Messwerte von $D$")
ax.plot(alpha_conti, D_alpha(alpha_conti, D1.nominal_value, D2.nominal_value), label=r"${:.4f}\, \frac{{\updelta t^3}}{{m_i^2}}\, \alpha^2 + {:.4f}\, \frac{{a^2}}{{\updelta t}}$".format(D1.nominal_value, D2.nominal_value))
ax.legend(loc="best")
fig.savefig("D_alpha.pdf")
