import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.optimize as sc
from uncertainties import *

_lambda = np.arange(0.005, 0.1, 0.005)
omega_square = []

for i in _lambda:
        f, time, omega2 = np.loadtxt("../Daten/omega_square%.3f.csv" %i, unpack=True)
        omega_square.append(omega2)

omega_square = np.array(omega_square)

#Plot lambda - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(_lambda, omega_square[:,10], ".")
ax.set_xlabel(r"$\lambda$")
ax.set_ylabel(r"$\langle \overline{\omega}^2 \rangle$")
fig.savefig("lambda-omega_square.pdf")

#Plot Zeit - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, omega_square[2], ".")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\langle \overline{\omega}^2 \rangle$")
fig.savefig("time-omega_square.pdf")

#Plot Zeit & lambda - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
im = ax.imshow(omega_square, interpolation="nearest", origin="lower")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\lambda$")
cb = fig.colorbar(im, ax = ax)
cb.set_label(r"$\langle \overline{\omega(\lambda, t)}^2 \rangle$")
fig.savefig("omega_square3d.pdf")
