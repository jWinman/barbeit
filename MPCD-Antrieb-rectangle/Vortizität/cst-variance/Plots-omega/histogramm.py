import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

alpha = ["0.010", "0.020", "0.030", "0.040", "0.050", "0.060", "0.070", "0.080", "0.090","0.100"]
binbreite = 1e-3
number = 500


for time in np.arange(15, 20):
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        for i in [alpha[0], alpha[-1]]:
                print(i)

                x, y, omega = np.loadtxt("../Daten/omega{}-{}.csv".format(i, time), unpack=True)
                x, y, u_x, u_y = np.loadtxt("../Daten/u{}-{}.csv".format(i, time), unpack=True)

                norm = np.mean((u_x**2 + u_y**2))
                omega = omega / norm
                print(max(omega))
                haufigkeit = np.zeros(number)
                bins = np.arange(0, number)
                for counter in omega:
                        if (counter >= number * binbreite):
                                continue
                        k = 0
                        while (counter >= k * binbreite):
                                k += 1
                        haufigkeit[k - 1] += 1 / len(omega)
                
                ax.plot(bins, haufigkeit, "+", label=r"$\alpha = {}$".format(i))
                ax.set_xlabel(r"$\frac{\omega^2}{\langle v_{c, \mathrm{cm}} \rangle}$")
                ax.set_ylabel(r"$\mathrm{frequency} / \%$")
                ax.legend(loc="best")

#        plt.xticks([0, 20, 40, 60, 80, 100], [0, 20 * binbreite, 40 * binbreite, 60 * binbreite, 80 * binbreite, 100 * binbreite])
        fig.savefig("histogramm{}-{}.pdf".format(time, i))

