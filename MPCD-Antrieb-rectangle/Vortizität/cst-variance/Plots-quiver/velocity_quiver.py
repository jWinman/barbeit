import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

alpha = "0.100"
for i in np.arange(30):
        # quiver
        x, y, ux, uy = np.loadtxt("../Daten/u{}-{}.csv".format(alpha, i), unpack=True)
        norm = np.sqrt(ux**2 + uy**2)
        x_o, y_o, omega = np.loadtxt("../Daten/omega{}-{}.csv".format(alpha, i), unpack=True)
        omega = np.array(np.split(omega, x_o[-1] + 1)).T

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
        ax.set_xlabel("$x / a$")
        ax.set_ylabel("$y / a$")
        im = ax.imshow(omega / 1e-3, origin="lower")
        cb = fig.colorbar(im, ax = ax, orientation="horizontal")
        cb.set_label(r"$\omega^2 / (10^{-3}\, \updelta t^2)$")
        fig.savefig("quiver{}-{}.pdf".format(alpha, i))
