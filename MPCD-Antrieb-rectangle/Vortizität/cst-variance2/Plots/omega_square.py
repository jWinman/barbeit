import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.optimize as sc
from uncertainties import *

alpha = np.arange(1,20)
time = np.arange(20)

omega_square = []

#Normierung mit <u^2>
for i in alpha:
        f, time, omega2 = np.loadtxt("../Daten/omega_square%d.csv" %i, unpack=True)
        for j in time:
                x, y, u_x, u_y = np.loadtxt("../Daten/u%d-%d.csv" %(i, j), unpack=True)
                norm = u_x**2 + u_y**2
                omega2[j] /= np.mean(norm)
        omega_square.append(omega2)

"""
#Normierung mit alpha
for i in alpha:
        f, time, omega2 = np.loadtxt("../Daten/omega_square%d.csv" %i, unpack=True)
        omega2 /= i**2
        omega_square.append(omega2)
"""
omega_square = np.array(omega_square)

#Plot alpha - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(alpha, omega_square[:,10], ".", label="SRD-Messwerte bei $t = 10 \cdot 5000$")
ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$\frac{\langle \omega^2 \rangle}{\alpha^2}$")
ax.legend(loc="best")
fig.savefig("alpha-omega_square_rectangle.pdf")

#Plot Zeit - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(time, omega_square[2], ".")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\langle \overline{\omega}^2 \rangle$")
fig.savefig("time-omega_square.pdf")

#Plot Zeit & alpha - Omega^2
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
im = ax.imshow(omega_square[:,:20], interpolation="nearest", origin="lower")
ax.set_xlabel("$t$")
ax.set_ylabel(r"$\alpha$")
plt.xticks([0, 5, 10, 15], [0, r"$5\cdot6000$", r"$10\cdot6000$", r"$15\cdot6000$"])
plt.yticks([0, 5, 10, 15], [0, 0.25, 0.5, 0.75])
cb = fig.colorbar(im, ax = ax)
cb.set_label(r"$\frac{1}{\alpha} \langle \omega(\alpha, t)^2 \rangle$")
fig.savefig("omega_square3d_rectangle.pdf")
