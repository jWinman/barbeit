import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

alpha = "20"
for i in np.arange(30):
        # quiver
        x, y, ux, uy = np.loadtxt("../Daten/u{}-{}.csv".format(alpha, i), unpack=True)
        norm = np.sqrt(ux**2 + uy**2)
        x_o, y_o, omega = np.loadtxt("../Daten/omega{}-{}.csv".format(alpha, i), unpack=True)
        omega = np.array(np.split(omega / np.mean(norm), x_o[-1] + 1)).T

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
        norm = np.array(np.split(norm / np.mean(norm), x[-1] + 1)).T
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")
        im = ax.imshow(norm, origin="lower")
        cb = fig.colorbar(im, ax = ax, orientation="horizontal")
        cb.set_label(r"$\omega^2$")
        fig.savefig("quiver{}-{}.pdf".format(alpha, i))
