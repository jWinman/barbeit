#include <iostream>
#include <cstdio>
#include <cmath>
#include <random>

#include "cellgrid.h"
#include "particle.h"

using namespace std;



Particle* createparticles(int N, double xright, double yright, double T)
{
	srand((unsigned) time(NULL));
	default_random_engine generator;
	normal_distribution<double> distribution(0, 1);

	Particle *array = new Particle[N];
	for (int i = 0; i < N; i++)
	{
		array[i] = Particle((double) rand() / (RAND_MAX + 1.0) * xright, (double) rand() / (RAND_MAX + 1.0) * yright, sqrt(T) * distribution(generator), sqrt(T) * distribution(generator), 1, 2 * M_PI * rand() / (RAND_MAX + 1.0));
	}
	return array;
}

void work(int NP, double T, double delta_t, double alpha, double variance, double x, double y, int measure_time, int measure_intervall, double ****mean_x_y, double ***omega, double ***particle_dens, bool on_off, double *theta_variance, double *n_variance, double *r_variance)
{
	srand((unsigned) time(NULL));
	// Teilchen und Zellen erstellen
	Particle *teilchenarray = createparticles(NP, x, y, T);
	CellGrid zellenarray(int(x), int(y), 1, NP);

	// mittlere Teilchenzahl	
	double M = (double) NP / (x * y);

	//Arrays für Varianzen
//	double *theta_0 = new double[NP]();
//	double *x_0 = new double[NP]();
//	double *y_0 = new double[NP]();

	// Ausleseteil:
	for (int h = 0; h < measure_intervall; h++)
	{
		// Äquilibrierung
		for (int w = 0; w < 5000; w++)
		{
/*			if (h == 1)
			{
				for (int k = 0; k < NP; k++)
				{
				if (w == 0)
				{
					theta_0[k] = teilchenarray[k].Get_theta();
					x_0[k] = teilchenarray[k].GetPos('x');
					y_0[k] = teilchenarray[k].GetPos('y');
				}
				theta_variance[w] += (teilchenarray[k].Get_theta() - theta_0[k]) * (teilchenarray[k].Get_theta() - theta_0[k]) / NP;
				n_variance[w] += cos(teilchenarray[k].Get_theta() - theta_0[k]) / NP;
				r_variance[w] += ((teilchenarray[k].GetPos('x') - x_0[k]) * (teilchenarray[k].GetPos('x') - x_0[k]) + (teilchenarray[k].GetPos('y') - y_0[k]) * (teilchenarray[k].GetPos('y') - y_0[k])) / NP;
				}
			}
*/
			zellenarray.reset_all();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.drive_all(teilchenarray, NP, delta_t, alpha, variance);
			zellenarray.mean(T, M);
			zellenarray.algorithm(0, x, 0, y, delta_t, on_off);
		}

		for (int w = 0; w < measure_time; w++)
		{
			// Algorithmus nochmal ausführen
			zellenarray.reset_all();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.drive_all(teilchenarray, NP, delta_t, alpha, variance);
			zellenarray.mean(T, M);
			zellenarray.algorithm(0, x, 0, y, delta_t, on_off);
			zellenarray.reset_particle_array();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.mean(T, M);

			// Daten in Arrays schreiben 

			//Geschwindigkeitsfeld erstellen
			for (int i = 0; i < int(x) + 1; i++)
			{
				for (int j = 0; j < int(y) + 1; j++)
				{
					mean_x_y[h][i][j][0] += zellenarray.Getux(i, j) / measure_time;
					mean_x_y[h][i][j][1] += zellenarray.Getuy(i, j) / measure_time;
					particle_dens[h][i][j] += zellenarray.quantity(i, j) / ((double) measure_time);
					if (i < int(x) && j < int(y))
					{
						double omega_tmp = (zellenarray.Getuy(i + 1, j) - zellenarray.Getuy(i, j)) - (zellenarray.Getux(i, j + 1) - zellenarray.Getux(i, j));
						omega[h][i][j] += omega_tmp / measure_time;
					}
				}
			}
		}
	}
	delete teilchenarray;
}

int main(int argc, char *argv[])
{
	// Initialisiert rand()
	srand((unsigned) time(NULL));
	
	// Anfangs- u. Randbedingungen festlegen
	int NP = 30000;
	double T = 0.01;
	double x = 100, y = 30;
	int measure_time = 1000;
	int measure_intervall = 3;
	double delta_t = 1;
	double alpha = 0.01;
	double variance = 0.01;
	bool on_off = true;
/*	if (*argv[1] == 'Y')
	{
		on_off = true;
	}
	else if (*argv[1] == 'N')
	{
		on_off = false;
	}
*/

	// Array in denen Mittelwert der Geschwindigkeiten bei der Auslesephase gespeichert werden
	double *theta_variance = new double[5000]();
	double *n_variance = new double[5000]();
	double *r_variance = new double[5000]();

	double ****mean_x_y = new double***[measure_intervall]();
	double ***omega = new double**[measure_intervall]();
	double *omega_square = new double[measure_intervall]();
	double ***particle_dens = new double**[measure_intervall]();

	for (int h = 0; h < measure_intervall; h++)
	{
		mean_x_y[h] = new double**[int(x) + 1];
		omega[h] = new double*[int(x) + 1];
		particle_dens[h] = new double*[int(x) + 1]();
		for (int i = 0; i < int(x) + 1; i++)
		{
			mean_x_y[h][i] = new double*[int(y) + 1]();
			omega[h][i] = new double[int(y) + 1]();
			particle_dens[h][i] = new double[int(y) + 1]();
			for (int j = 0; j < int(y) + 1; j++)
			{
				mean_x_y[h][i][j] = new double[2]();

			}
		}
	}

	//Äquilibrieren und Messen
	work(NP, T, delta_t, alpha, variance, x, y, measure_time, measure_intervall, mean_x_y, omega, particle_dens, on_off, theta_variance, n_variance, r_variance);

	//Output
	printf("Output\n");
	char name1[256];
	sprintf(name1, "Daten/omega_square%s.csv", argv[1]);
	FILE *file1 = fopen(name1, "w");
	for (int h = 0; h < measure_intervall; h++)
	{
		char name2[256];
		char name3[256];
		char name4[256];
		sprintf(name2, "Daten/u%s-%d.csv", argv[1], h);
		sprintf(name3, "Daten/omega%s-%d.csv", argv[1], h);
		sprintf(name4, "Daten/dichte%s-%d.csv", argv[1], h);
		FILE *file2 = fopen(name2, "w");
		FILE *file3 = fopen(name3, "w");
		FILE *file4 = fopen(name4, "w");
		for (int i = 0; i < int(x) + 1; i++)
		{
			for (int j = 0; j < int(y) + 1; j++)
			{
				fprintf(file2, "%d %d %.15f %.15f\n", i, j, mean_x_y[h][i][j][0], mean_x_y[h][i][j][1]);
				fprintf(file4, "%.20f ", particle_dens[h][i][j]);
				if (i < int(x) && j < int(y))
				{
					fprintf(file3, "%d %d %.15f\n", i, j, omega[h][i][j] * omega[h][i][j]);
					omega_square[h] += omega[h][i][j] * omega[h][i][j] / ((int(x) - 1) * int(y));
				}
			}
			fprintf(file4, "\n");
		}
		fclose(file2);
		fclose(file3);
		fclose(file4);
		fprintf(file1, "%s %d %.20f\n", argv[1], h, omega_square[h]);
	}
	fclose(file1);
/*
	char name5[256];
	char name6[256];
	char name7[256];
	sprintf(name5, "Daten/theta_variance%s.csv", argv[1]);
	sprintf(name6, "Daten/n_variance%s.csv", argv[1]);
	sprintf(name7, "Daten/r_variance%s.csv", argv[1]);
	FILE *file5 = fopen(name5, "w");
	FILE *file6 = fopen(name6, "w");
	FILE *file7 = fopen(name7, "w");
	for (int time = 0; time < 5000; time++)
	{
		fprintf(file5, "%.10f\n", theta_variance[time]);
		fprintf(file6, "%.10f\n", n_variance[time]);
		fprintf(file7, "%.10f\n", r_variance[time]);
	}
	fclose(file5);
	fclose(file6);
	fclose(file7);
	return 0;
*/
}
