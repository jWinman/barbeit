#include <cmath>
#include <iostream>

#include "particle.h"

double Particle::GetPos(char pos)
{
	switch(pos)
	{
		case 'x': return _x;
		case 'y': return _y;
		default: return 0;
	}
}

double Particle::Getv(char pos)
{
	switch(pos)
	{
		case 'x': return _v_x;
		case 'y': return _v_y;
		default: return  0;
	}
}

void Particle::x_border(double xleft, double xright, double time) //xright > x > xleft
{
	if (_x < xleft || _x >= xright)
	{
		_v_x = -_v_x;
		_v_y = -_v_y;
		streaming(time);
	}
}

void Particle::y_border(double ydown, double yup, double time)
{
	if (_y < ydown || _y >= yup)
	{
		_v_x = -_v_x;
		_v_y = -_v_y;
		streaming(time);
	}
}

void Particle::streaming(double time)
{
	_y += time * _v_y;
	_x += time * _v_x;
}

void Particle::acceleration(double time, double force_x)
{
	_v_x += force_x / _mass * time;
}

void Particle::drive(double time, double force, double theta)
{
	_theta += theta;
	_v_x += force / _mass * cos(_theta) * time;
	_v_y += force / _mass * sin(_theta) * time;
}

void Particle::collisionSRD(double vx_mittel, double vy_mittel, int R)
{
	double v_x_temp = _v_x;
	_v_x = vx_mittel - R * (_v_y - vy_mittel);
	_v_y = vy_mittel + R * (v_x_temp - vx_mittel);
}

void Particle::shift(double a0_x, double a0_y, double xleft, double xright)
{
	double l_x = fabs(xleft - xright);
	_x += a0_x;
	if (_x < xleft) _x += l_x;
	else if (_x > xright) _x -= l_x;
	_y += a0_y;
}













