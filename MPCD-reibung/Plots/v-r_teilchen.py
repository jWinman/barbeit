import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.optimize as sc
from uncertainties import *

v_x_001, v_y_001 = np.loadtxt("../Daten/v_teilchen-0.001.csv", unpack=True)
v_x_01, v_y_01 = np.loadtxt("../Daten/v_teilchen-0.01.csv", unpack=True)
v_x_0005, v_y_0005 = np.loadtxt("../Daten/v_teilchen-0.0005.csv", unpack=True)
v_x_0, v_y_0 = np.loadtxt("../Daten/v_teilchen-0.csv", unpack=True)
v_001 = np.sqrt(v_x_001**2 + v_y_001**2)
v_01 = np.sqrt(v_x_01**2 + v_y_01**2)
v_0005 = np.sqrt(v_x_0005**2 + v_y_0005**2)
v_0 = np.sqrt(v_x_0**2 + v_y_0**2)

print(r"alpha = 0.0005")
print(np.mean(v_0005[:5000]))
print(np.mean(v_0005[:10000]))
print(np.mean(v_0005))

print(r"alpha = 0.001")
print(np.mean(v_001[:5000]))
print(np.mean(v_001[:10000]))
print(np.mean(v_001))

print(r"alpha = 0.01")
print(np.mean(v_01[:5000]))
print(np.mean(v_01[:10000]))
print(np.mean(v_01))

print(np.mean(v_y_01[:5000]))
print(np.mean(v_y_01[:10000]))
print(np.mean(v_y_01))

print(r"alpha = 0")
print(np.mean(v_0[:5000]))
print(np.mean(v_0[:10000]))
print(np.mean(v_0))
