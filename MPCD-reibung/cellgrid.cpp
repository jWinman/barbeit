#include <cmath>
#include <iostream>
#include <ctime>
#include <random>

#include "particle.h"
#include "cellgrid.h"

double boxmuller2(){
	const double pi = 4 * atan(1.0);
	double a=(double)rand()/(RAND_MAX+1.0);
	double b=(double)rand()/(RAND_MAX+1.0);
	a=sqrt(-2*log(a))*cos(2*pi*b);
	return a;
}

// Konstruktor
CellGrid::CellGrid(int Nx, int Ny, int a, int N_Particle) : 
	_Nx(Nx), 
	_Ny(Ny + 1), 
	_a(a), 
	_N_Particle(N_Particle)
{
	// Speicher anfordern
	_particle_array = new Particle***[_Nx]();
	_u = new double**[_Nx]();
	_Particle_per_cell = new int*[_Nx]();
	_R = new int*[_Nx]();
	for (int i = 0; i < _Nx; i++)
	{
		_particle_array[i] = new Particle**[_Ny]();
		_u[i] = new double*[_Ny]();
		_Particle_per_cell[i] = new int[_Ny]();
		_R[i] = new int[_Ny]();
		for (int j = 0; j < _Ny; j++)
		{
			_particle_array[i][j] = new Particle*[int(_N_Particle / 3)]();
			_u[i][j] = new double[2]();
			_u[i][j][0] = 0;
			_u[i][j][1] = 0;
		}
	}
}

void CellGrid::ghost_particles(int i, int j, double T, double M)
{
	using namespace std;
	double sigma = sqrt((M - _Particle_per_cell[i][j]) * T);
	default_random_engine generator (rand());
	normal_distribution<double> distribution(0, sigma);
	_u[i][j][0] = (_u[i][j][0] + distribution(generator)) / M;
	_u[i][j][1] = (_u[i][j][1] + distribution(generator)) / M;
}

void CellGrid::reset_all()
{
	// Nullsetzen und Startwerte setzen
	_shift_x = _a / 2. - rand() / (RAND_MAX + 1.0);
	_shift_y = _a / 2. - rand() / (RAND_MAX + 1.0);
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			_Particle_per_cell[i][j] = 0;
			_R[i][j] = ((int) rand() % 2 == 0) ? -1 : 1;
		}
	}
}

void CellGrid::reset_particle_array()
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			_Particle_per_cell[i][j] = 0;
		}
	}
}

void CellGrid::acceleration_0(Particle *array, int length, double delta_t, double force_x)
{
	array[length - 1].acceleration(delta_t, force_x);
}

void CellGrid::drive_all(Particle *array, int length, double delta_t, double force, double variance)
{
	using namespace std;
	default_random_engine generator (rand());
	normal_distribution<double> distribution(0, variance);
	for (int k = 0; k < length; k++)
	{
		array[k].drive(delta_t, force, distribution(generator));
	}
}

void CellGrid::fill_up_particle_array(Particle *array, int length, double xright)
{
	for (int k = 0; k < length; k++) // Schleife über alle Teilchen
	{
		// Auffüllen in _particle_array unter Beachtung des shifts
		array[k].shift(_shift_x, _shift_y, 0, xright); // shiften
		int tmpx = array[k].GetPos('x') / _a;
		int tmpy = 0;
		if (_shift_y < 0)
		{
			tmpy = array[k].GetPos('y') / _a + ceil(fabs(_shift_y));
		}
		else
		{
			tmpy = array[k].GetPos('y') / _a;
		}

		_particle_array[tmpx][tmpy][_Particle_per_cell[tmpx][tmpy]] = &array[k];
		_Particle_per_cell[tmpx][tmpy]++;
		array[k].shift(-_shift_x, -_shift_y, 0, xright); //zurückshiften
	}
}

void CellGrid::mean(double T, int M)
{
	// Berechnung von u, Ghost-Particles hinzufügen
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			_u[i][j][0] = _u[i][j][1] = 0;
			for (int k = 0; k < _Particle_per_cell[i][j]; k++) // Schleife über Teilchen in der Zelle (i, j)
			{
				_u[i][j][0] += _particle_array[i][j][k]->Getv('x');
				_u[i][j][1] += _particle_array[i][j][k]->Getv('y');
			}

			if ((j == 0 || j == _Ny - 1) && M > _Particle_per_cell[i][j] && _Particle_per_cell[i][j] > 0)
			{
				ghost_particles(i, j, T, M); // Ghost- / Wall-particles hinzufügen
			}
			else if (_Particle_per_cell[i][j] > 0)
			{
				_u[i][j][0] /= _Particle_per_cell[i][j];
				_u[i][j][1] /= _Particle_per_cell[i][j];
			}
		}
	}
}

void CellGrid::algorithm(double xleft, double xright, double ydown, double yup, double delta_t)
{
	for (int i = 0; i < _Nx; i++) // Schleife über x-Zellen
	{
		for (int j = 0; j < _Ny; j++) // Schleife über y-Zellen
		{
			for (int k = 0; k < _Particle_per_cell[i][j]; k++)
			{
				_particle_array[i][j][k]->collisionSRD(_u[i][j][0], _u[i][j][1], _R[i][j]);
				_particle_array[i][j][k]->streaming(delta_t);
				_particle_array[i][j][k]->y_border(ydown, yup, delta_t);
				_particle_array[i][j][k]->x_border(xleft, xright);
				
			}
		}
	}
}

CellGrid::~CellGrid()
{
	for (int i = 0; i < _Nx; i++)
	{
		for (int j = 0; j < _Ny; j++)
		{
			delete[] _particle_array[i][j];
			delete[] _u[i][j];
		}
		delete[] _particle_array[i];
		delete[] _u[i];
		delete[] _Particle_per_cell[i];
		delete[] _R[i];
	}
	delete[] _particle_array;
	delete[] _u;
	delete[] _Particle_per_cell;
	delete[] _R;
}
