#include "particle.h"

#ifndef CELL_H
#define CELL_H

class CellGrid
{
	private:
		int _Nx, _Ny; // Anzahl der Zellen in x- und y-Richtung
		double _a;  // Gitterkonstante und Abmessungen des Gitters
		int _N_Particle; // Gesamtanzahl der Teilchen
		int **_Particle_per_cell; // Anzahl der Teilchen pro Zelle
		Particle ****_particle_array; // Teilchen / Zelle in dem Gitter
		double ***_u; // Mittelwerte der Geschwindigkeiten
		int **_R; // Werte für die Rotationsmatrix
		double _shift_x; // Werte für die Verschiebung des Gitters
		double _shift_y;
	public:
		CellGrid(int Nx, int Ny, int a, int N_Particle); // Konstruktur
		void ghost_particles(int i, int j, double T, double M); // berechnet Wandteilchen beim Mittelwert
		void reset_all(); // _shift, _R initialisieren und _Particle_per_cell(i, j) nullsetzen
		void reset_particle_array(); // nur _Particle_per_cell(i, j) nullsetzen
		void fill_up_particle_array(Particle *array, int length, double xright); // füllt _particle_array(i, j) mit Teilchen aus array
		void mean(double T, int M); // Berechnet Mittelwert unter berücksichtigung der Ghost-Particle
		void acceleration_0(Particle *array, int length, double delta_t, double force); // Beschleunigt Teilchen mit der Kraft force in x-Richtung
		void drive_all(Particle *array, int length, double delta_t, double force, double variance); //Beschleunigt alle Teilchen mit der Kraft force in random()-Richtung
		void algorithm(double xleft, double xright, double ydown, double yup, double time); // führt den eigentlichen Algorithmus aus collision und streaming aus
		double Getux(int i, int j) {return _u[i][j][0];} // Mittelwert u_x der Zelle (i,j) zurückgeben
		double Getuy(int i, int j) {return _u[i][j][1];} // Mittelwert u_y der Zelle (i,j) zurückgeben
		int quantity(int i, int j) {return _Particle_per_cell[i][j];}; // Anzahl an Teilchen in Zelle (i, j)
		~CellGrid(); // DESTRUCTION
};

#endif
