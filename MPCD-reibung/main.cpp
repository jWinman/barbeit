#include <iostream>
#include <cstdio>
#include <cmath>
#include <random>

#include "cellgrid.h"
#include "particle.h"

using namespace std;



Particle* createparticles(int N, double xright, double yright, double T)
{
	srand((unsigned) time(NULL));
	default_random_engine generator;
	normal_distribution<double> distribution(0, 1);

	Particle *array = new Particle[N];
	array[N - 1] = Particle(xright / 2, yright / 2, 0, 0, 1, 0);
	for (int i = 0; i < N - 1; i++)
	{
		array[i] = Particle((double) rand() / (RAND_MAX + 1.0) * xright, (double) rand() / (RAND_MAX + 1.0) * yright, sqrt(T) * distribution(generator), sqrt(T) * distribution(generator), 1, 2* M_PI * rand() / (RAND_MAX + 1.0));
	}
	return array;
}

void work(int NP, double T, double delta_t, double force_x, double alpha, double variance, double x, double y, int measure_time, int measure_intervall, double ****mean_x_y, double *v_mean_x, double *v_mean_y, double *r_mean_x, double *r_mean_y)
{
	srand((unsigned) time(NULL));
	// Teilchen und Zellen erstellen
	Particle *teilchenarray = createparticles(NP, x, y, T);
	CellGrid zellenarray(int(x), int(y), 1, NP);

	// mittlere Teilchenzahl	
	double M = (double) NP / (x * y);

	// Äquilibrierung ohne das letzte Teilchen
	for (int t = 0; t < 5000; t++)
	{
		zellenarray.reset_all();
		zellenarray.fill_up_particle_array(teilchenarray, NP - 1, x);
		zellenarray.drive_all(teilchenarray, NP - 1, delta_t, alpha, variance);
		zellenarray.mean(T, M);
		zellenarray.algorithm(0, x, 0, y, delta_t);
	}

	// Ausleseteil:
	size_t time = 0;
	
	for (int h = 0; h < measure_intervall; h++)
	{
		for (int w = 0; w < measure_time; w++)
		{
			// Algorithmus mit letzten Teilchen und Kraft auf dieses Teilchen ausführen
			v_mean_x[time] = teilchenarray[NP - 1].Getv('x');
			v_mean_y[time] = teilchenarray[NP - 1].Getv('y');
			r_mean_x[time] = teilchenarray[NP - 1].GetPos('x');
			r_mean_y[time] = teilchenarray[NP - 1].GetPos('y');
			time++;

			zellenarray.reset_all();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.drive_all(teilchenarray, NP, delta_t, alpha, variance);
			zellenarray.acceleration_0(teilchenarray, NP, delta_t, force_x);
			zellenarray.mean(T, M);
			zellenarray.algorithm(0, x, 0, y, delta_t);
			zellenarray.reset_particle_array();
			zellenarray.fill_up_particle_array(teilchenarray, NP, x);
			zellenarray.mean(T, M);

			// Daten in Arrays schreiben
			
			//Geschwindigkeitsfeld erstellen
			for (int i = 0; i < int(x); i++)
			{
				for (int j = 0; j < int(y) + 1; j++)
				{
					mean_x_y[h][i][j][0] += zellenarray.Getux(i, j) / measure_time;
					mean_x_y[h][i][j][1] += zellenarray.Getuy(i, j) / measure_time;
				}
			}
		}
	}
	delete teilchenarray;
}

int main(int argc, char *argv[])
{
	// Initialisiert rand()
	srand((unsigned) time(NULL));
	
	// Anfangs- u. Randbedingungen festlegen
	int NP = 30000;
	double T = 0.01;
	double x = 100, y = 30;
	int measure_time = 2000;
	int measure_intervall = 25;
	double delta_t = 0.1;
	double force_x = 0.05;
	double alpha = atof(argv[1]);
	double variance = 0.01;

	// Array in denen Mittelwert der Geschwindigkeiten bei der Auslesephase gespeichert werden
	double *v_mean_x = new double[measure_intervall * measure_time]();
	double *v_mean_y = new double[measure_intervall * measure_time]();
	double *r_mean_x = new double[measure_intervall * measure_time]();
	double *r_mean_y = new double[measure_intervall * measure_time]();

	double ****mean_x_y = new double***[measure_intervall]();

	for (int h = 0; h < measure_intervall; h++)
	{
		mean_x_y[h] = new double**[int(x)];
		for (int i = 0; i < int(x); i++)
		{
			mean_x_y[h][i] = new double*[int(y) + 1]();
			for (int j = 0; j < int(y) + 1; j++)
			{
				mean_x_y[h][i][j] = new double[2]();

			}
		}
	}

	//Äquilibrieren und Messen
	work(NP, T, delta_t, force_x, alpha, variance, x, y, measure_time, measure_intervall, mean_x_y, v_mean_x, v_mean_y, r_mean_x, r_mean_y);

	//Output
	printf("Output\n");
	for (int h = 0; h < measure_intervall; h++)
	{
		// Geschwindigkeit u in Datei schreiben
		char name1[256];
		sprintf(name1, "Daten/u%s-%d.csv", argv[1], h);
		FILE *file1 = fopen(name1, "w");
		for (int i = 0; i < int(x); i++)
		{
			for (int j = 0; j < int(y) + 1; j++)
			{
				fprintf(file1, "%d %d %.15f %.15f\n", i, j, mean_x_y[h][i][j][0], mean_x_y[h][i][j][1]);
			}
		}
		fclose(file1);
	}

	char name2[256];
	char name3[256];
	sprintf(name2, "Daten/v_teilchen-%s.csv", argv[1]);
	sprintf(name3, "Daten/r_teilchen-%s.csv", argv[1]);
	FILE *file2 = fopen(name2, "w");
	FILE *file3 = fopen(name3, "w");
	for (int time = 0; time < measure_intervall * measure_time; time++)
	{
		fprintf(file2, "%.10f %.10f\n", v_mean_x[time], v_mean_y[time]);
		fprintf(file3, "%.10f %.10f\n", r_mean_x[time], r_mean_y[time]);
	}
	fclose(file2);
	fclose(file3);
	return 0;
}
